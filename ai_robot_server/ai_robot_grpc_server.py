from multiprocessing import Process
from concurrent import futures
import time
import atexit
import grpc

from ai_robot_server.ai_robot_controller import AIRobotController
import proto.RobotSystemCommunication_pb2 as rsc_pb2
import proto.RobotSystemCommunication_pb2_grpc as rsc_pb2_grpc


class AIRobotGRPCServer(rsc_pb2_grpc.RobotFrontendServicer):
    """
    Class to send the robot backend motor commands
    to the robot's motor controller via serial port
    """
    def __init__(self):
        self._server_port = 50053
        self._robot_controller = AIRobotController()

        self._robot_server = grpc.server(futures.ThreadPoolExecutor(max_workers=1))
        rsc_pb2_grpc.add_RobotFrontendServicer_to_server(self, self._robot_server)
        self._robot_server.add_insecure_port('[::]:{}'.format(self._server_port))

        self._process = None
        atexit.register(self.close_server)

    def _start_server(self):
        self._robot_server.start()
        print('=== Robot Frontend server running at port {} ...'.format(self._server_port))
        try:
            while True:
                time.sleep(60*60*60)
        except KeyboardInterrupt:
            self._robot_server.stop(0)
            print('Robot server Stopped ...')

    def start_server(self):
        """
        Start the gRPC server to listen to calls from
        Robot Backend
        """
        self._process = Process(target=self._start_server)
        self._process.start()

    def close_server(self):
        """
        Stop gRPC server
        """
        print('=== Closing Robot server ...')
        self._robot_controller.close()
        self._process.terminate()


    def MakeAction(self, request, context):
        """
        Send motor values to the robot's motors
        """
        left_motor_value = request.leftMotorAction
        right_motor_value = request.rightMotorAction
        command_ok = self._robot_controller.set_motors(
            left_motor_value, right_motor_value)
        if command_ok is not None:
            status = rsc_pb2.OK
        else:
            status = rsc_pb2.ERROR

        return rsc_pb2.RobotActionResponse(status=status)
