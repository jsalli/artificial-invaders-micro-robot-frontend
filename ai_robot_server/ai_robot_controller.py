import pigpio


DIR_RIGHT_PIN = 6   # DIRA_PIN
DIR_LEFT_PIN = 5    # DIRB_PIN
PWM_RIGHT_PIN = 12  # PWMA_PIN
PWM_LEFT_PIN = 13   # PWMB_PIN

PWM_FREQ = 100

FORWARD_DIRECTION = 1
BACKWARD_DIRECTION = 0

MAX_DUTY = 1000000


class AIRobotController:
    """
    RobotController handles the writing of GPIO pins
    """
    def __init__(self):
        """
        Setting up GPIOs
        """
        self._pi = pigpio.pi()

        # DIRs
        self._pi.set_mode(DIR_LEFT_PIN, pigpio.OUTPUT) # DIRB
        self._pi.set_mode(DIR_RIGHT_PIN, pigpio.OUTPUT) # DIRA
        # PWMs
        self._pi.set_mode(PWM_LEFT_PIN, pigpio.OUTPUT) # PWMB
        self._pi.set_mode(PWM_RIGHT_PIN, pigpio.OUTPUT) # PWMA

    def _duty_cycle(self, value):
        return int(abs(value) / 255 * MAX_DUTY)

    def set_motors(self, left_motor_value, right_motor_value):
        """
        Set direction pins and set PWM values
        """
        # Settings dirs
        if left_motor_value < 0:
            self._pi.write(DIR_LEFT_PIN, BACKWARD_DIRECTION)
        else:
            self._pi.write(DIR_LEFT_PIN, FORWARD_DIRECTION)
        if right_motor_value < 0:
            self._pi.write(DIR_RIGHT_PIN, BACKWARD_DIRECTION)
        else:
            self._pi.write(DIR_RIGHT_PIN, FORWARD_DIRECTION)

        left_duty = self._duty_cycle(left_motor_value)
        right_duty = self._duty_cycle(right_motor_value)
        print(f'left_duty: {left_duty:9d}, right_duty: {right_duty:9d}', end='\r')
        self._pi.hardware_PWM(
            PWM_LEFT_PIN,
            PWM_FREQ,
            left_duty)
        # self._pi.write(DIR_LEFT_PIN, 1)
        self._pi.hardware_PWM(
            PWM_RIGHT_PIN,
            PWM_FREQ,
            right_duty)
        return True

    def close(self):
        """
        Set GPIOs as inputs
        """
        self._pi.set_mode(DIR_LEFT_PIN, pigpio.INPUT)
        self._pi.set_mode(DIR_RIGHT_PIN, pigpio.INPUT)
        self._pi.set_mode(PWM_LEFT_PIN, pigpio.INPUT)
        self._pi.set_mode(PWM_RIGHT_PIN, pigpio.INPUT)
