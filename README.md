# AI Robot based on Raspberry Pi

Read the summary of Micro Invaders from [here](https://github.com/robot-uprising-hq/ai-guide).

This repo has code for AI Robot which has Raspberry Pi Zero.

# Installation
Check the instruction from [here](docs/AI-Robot-Installation-On-Raspberry-Pi.md)

# Using this repo
## Drive the robot from remote client's keyboard
Check the instruction from [here](docs/Manually-Driving-The-AI-Robot.md)

## Drive the robot with AI Backend Connector
Check the instruction from [here](docs/AI-Backend-Connector-Driving-The-AI-Robot.md)
