## Flashing SD-card and starting Raspberry Pi
1. Install Raspberry Pi OS to your Raspberry Pi's SD-card.
1. Open `boot`-partition of the SD-card and type `touch ssh` in .terminal to create empty file called ssh. This opens ssh connection to Raspberry Pi.
1. Open the `wpa_supplicant.conf_TEMPLATE`-file in `raspberry_pi_files`-folder and set your Wifi's ssid and password. Save the edited file as `wpa_supplicant.conf` and copy it to the SD-card's partition called `boot`.
1. Copy the files from repo to the SD-cards `rootfs`-partition to folder `/home/pi/ai-robot`
1. Start Raspberry Pi with the SD-card.
1. Find Raspberry Pi's IP-address and log in. Default user is `pi` and password `raspberry`.

---

## Setting up Raspberry Pi
1. 
    ```
    sudo apt update && sudo apt upgrade -y
    ```
1. 
    ```
    sudo raspi-config
    ```
    In Raspi-Config change the following:
    - 7 Advanced Options -> A1 Expand Filesystem
    - 7 Advanced Options -> A3 Memory Split -> 16MB (The GPU doesn't need extra memory in this application) 
1. Restart Raspberry Pi and log back in.
1. Install Python virtualenv
    ```sh
    sudo apt install python3-pip -y
    sudo pip3 install virtualenv
    ```
1. Create Python virtual environment to `ai-robot`-folder and take it into use
    ```sh
    cd ai-robot
    virtualenv venv
    source venv/bin/activate
    ```
1. Install Python packages to Raspberry Pi
    ```sh
    pip install --upgrade pip
    pip install --upgrade setuptools
    pip install -r requirements-raspberrypi.txt
    ```
1. Install pigpio.
[Source](http://abyz.me.uk/rpi/pigpio/download.html)
    ```sh
    wget https://github.com/joan2937/pigpio/archive/master.zip
    unzip master.zip
    cd pigpio-master
    make
    sudo make install
    ```
