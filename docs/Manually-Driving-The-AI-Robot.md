# Install PC dependencies
1. Create Python virtual environment
    ```sh
    virtualenv venv
    source venv/bin/activate
    ```
1. Install Python packages for PC
    ```sh
    pip install --upgrade pip
    pip install --upgrade setuptools
    pip install -r requirements-pc.txt
    ```

---
# Manually test drive robot

## On Raspberry PI
1. Start the pigpiod program in background
    ```sh
    sudo pigpiod
    ```
1. 
    ```sh
    cd ai-robot
    source venv/bin/activate
    python ai_robot_server.py
    ```

## On PC
1. 
    ```sh
    source venv/bin/activate
    python ai_robot_remote_keyboard_driver.py -a=[ROBOT-IP-ADDRESS-WITHOUT-BRACKETS]
    ```
1. Use keyboard's W, A, S, D, Q, E to command the robot's motors.
