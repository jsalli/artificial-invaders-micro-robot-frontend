# On robot
sudo pigpiod
cd micro-robot-frontend
source venv/bin/activate
python robot_frontend_server.py

# On PC
source venv/bin/activate
python remote_keyboard_driver.py