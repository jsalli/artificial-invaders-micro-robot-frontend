from absl import app
from absl import flags
import time
from multiprocessing import Process, Value, Array, Manager
import grpc
from pynput import keyboard
from pynput.keyboard import Key

from ai_robot_client.ai_robot_client import AIRobotClient


flags.DEFINE_string(
    "robot_ip",
    "",
    "Specify the IP-address of AI Robot",
    short_name="a")

flags.DEFINE_string(
    "robot_port",
    "50053",
    "Specify the port of AI Robot",
    short_name="p")

FLAGS = flags.FLAGS


class KeyboardActions():
    def __init__(self):
        self._action = Value("i", 0)
        self._p = Process(target=self._start, args=(self._action,))

    def _start(self, shared_value):
        print("Starting process")
        try:
            with keyboard.Listener(
                    on_press=self._on_press,
                    on_release=self._on_release) as listener:
                listener.join()
        except Exception as error:
            print("====ERROR =====")
            print(error)
            print("====ERROR =====")

    def start(self):
        self._p.start()

    def _get_action_for_key(self, key_value):
        action = 0
        # Going forward
        if key_value == 'w':
            action = 1
        # Going down
        elif key_value == 's':
            action = 2
        # Turn sharply right
        elif key_value == 'd':
            action = 3
        # Turn sharply left
        elif key_value == 'a':
            action = 4
        # Turn right and go forward
        elif key_value == 'q':
            action = 5
        # Turn left and go forward
        elif key_value == 'e':
            action = 6

        return action

    def _on_press(self, key):
        key_value = None
        try:
            key_value = key.char
        except AttributeError:
            pass
        self._action.value = self._get_action_for_key(key_value)

    def _on_release(self, key):
        self._action.value = 0

        if key == Key.esc:
            #  Stop listener
            return False

    def get_action(self):
        return self._action.value

    def close(self):
        self._p.terminate()


def main(_):
    """
    Test communication between AI Robot and your PC.
    """
    robot_ip = FLAGS.robot_ip
    robot_port = FLAGS.robot_port

    keyboard_actions = KeyboardActions()
    keyboard_actions.start()

    ai_robot = AIRobotClient(robot_ip, robot_port)

    try:
        while True:
            action = keyboard_actions.get_action()
            status = ai_robot.make_action(action)

    except KeyboardInterrupt:
        print("Closing")
        keyboard_actions.close()
        status = ai_robot.make_action(0)
    except grpc.RpcError as error:
        status_code = error.code()
        if grpc.StatusCode.UNAVAILABLE == status_code:
            print("!!!!AI Robot server not reached!!!!")
    except Exception as error:
        print("ERROR")
        print(error)
    finally:
        print("Exiting")
        keyboard_actions.close()


if __name__ == '__main__':
    app.run(main)
