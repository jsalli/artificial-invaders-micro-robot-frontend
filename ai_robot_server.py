#! /home/pi/robotfrontend/venv/bin/python
import time

from ai_robot_server.ai_robot_grpc_server import AIRobotGRPCServer


def main():
    """
    Start backend communicator which gets
    motor values from robot backend
    """
    server = AIRobotGRPCServer()
    server.start_server()

    try:
        while True:
            time.sleep(60*60*60)

    except KeyboardInterrupt:
        server.close_server()
        print('Robot server Stopped ...')


if __name__ == '__main__':
    main()
